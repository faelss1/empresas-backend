const { connect,query } = require('../../utils/db/pg');
const { newToken } = require('../../utils/auth/auth');
//Utilizando sha1 sabendo que não é mais seguro apenas para propósitos de desenvolvimento
const sha1 = require('sha1');

const authUser = (req, res, next) => {
    if (!req.body.email || !req.body.password) {
        return res.status(401).json({
            success: false,
            errors: [
                "Invalid login credentials. Please try again."
            ]
        });
    }

    connect( (err,client) => {
        if (err) {
            res.status(500).json({status: 500, error: "Could not connect to postgres"});
        } else {
            const authUserSQL = `
                select 
                    id,
                    email,
                    investor_name,
                    city,
                    country,
                    balance,
                    portfolio,
                    portfolio_value,
                    first_access,
                    super_angel
                 from enterprises.investor where email = $1 and password = $2;
            `;
            query(client, authUserSQL, [ req.body.email,sha1(req.body.password) ], db => {
                if (!db.error) {
                    if (db.rows.length === 0) {
                        res.status(401).json({
                            success: false,
                            errors: ["Invalid login credentials. Please try again."]
                        });
                    } else {
                        const token = newToken(db.rows[0]);
                        res.set({'access-token': token});
                        res.status(200).json({
                            investor: db.rows[0],
                            enterprise: null,
                            success: true
                        });
                    }
                }
            });
        }
    });
};

module.exports = {
    authUser
};