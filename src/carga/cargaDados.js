const { connect,query } = require('../api/utils/db/pg');
const exampleData = require('../json/empresas');

(() => {
    connect( (err,client) => {
        if (err) {
            console.error("Failed to connect to postgres " + err);
        } else {
            let valuesBulkInsert = [];
            exampleData.enterprises.forEach(v => {
                const dataFormated = Object.values(v).map(v => {
                    if (v instanceof Object) {
                        return "$$" + JSON.stringify(v) + "$$";
                    }
                    return "$$" + v + "$$"
                });
                valuesBulkInsert.push('(' + dataFormated + ')')
            });
            const insertAllEnterprisesSQL = `
                insert into enterprises.enterprises (
                    id,
                    email_enterprise,
                    facebook,
                    twitter,
                    linkedin,
                    phone,
                    own_enterprise,
                    enterprise_name,
                    photo,
                    description,
                    city,
                    country,
                    value,
                    share_price,
                    enterprise_type
                )
                values
                ${valuesBulkInsert.toString()}
            `;
            query(client ,insertAllEnterprisesSQL ,[] ,db => {
                if (db.error) {
                    console.error(db.error);
                } else {
                    console.log("Ok");
                }
            })
        }
    });
})();