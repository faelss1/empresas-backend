const Express = require('express');
const db = require('./src/api/utils/db/pg');
const cors = require('cors');
const path = require('path');
const { json, urlencoded } = require('body-parser');
const enterprisesRouter = require('./src/api/res/enterprises/enterprises.router');
const userRouter = require('./src/api/res/user/user.router');
require('dotenv').config({path: path.resolve(__dirname + '/.env')});
const app = Express();

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: true }));

app.use('/api/v1/enterprises/', enterprisesRouter);
app.use('/api/v1/users/', userRouter);

app.use('*', (req, res) => {
    res.status(404).sendFile(path.join(__dirname, 'public/', '404.html'));
});


module.exports = {
    start: () => {
        app.listen('8000', () => {
           console.info('API listening on port 8000');
        });
    }
};

