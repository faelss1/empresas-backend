const { detailEnterprises,listEnterprises,filterEnterprises,handleNotFound } = require('./enterprises.controller');
const { protectRoute } = require('../../utils/auth/auth');
const { Router } = require('express');

const router = Router();

// Detalhamento
router
    .get('/:id(\\d+)/', protectRoute, detailEnterprises);

// Filtros
router
    .get('', protectRoute, filterEnterprises);

// Listagem
router
    .get('\\/?', protectRoute, listEnterprises);

//404
router
    .all('*', protectRoute, handleNotFound);


module.exports = router;