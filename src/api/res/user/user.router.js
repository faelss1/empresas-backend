const { authUser } = require('./user.controller');
const { Router } = require('express');

const router = Router();
// Procura usuario pelas credenciais
router
    .get('/auth/sign_in/', (req, res) => {
        res.status(405).json({
            success: false,
            errors: ["Use POST /sign_in to sign in. GET is not supported."]
        });
    })
    .post('/auth/sign_in/', authUser);

module.exports = router;