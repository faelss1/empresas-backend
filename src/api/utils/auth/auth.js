const jwt = require('jsonwebtoken');

const newToken = user => {
    return jwt.sign({
        id: user.id,
    }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES
    });
};

const verifyToken = token => {
    return new Promise( (resolve, reject) => {
       jwt.verify(token, process.env.JWT_SECRET, (err,jwt) => {
           if (err) return reject(err);
           resolve(jwt);
       });
    });
};

const protectRoute = async (req, res, next) => {
    const token = req.get('access-token');
    let payload;
    try {
        payload = await verifyToken(token);
    } catch (e) {
        return res.status(401).json({
            errors: [
                "You need to sign in or sign up before continuing."
            ]
        });
    }
    if (!token || !payload) {
        return res.status(401).json({
            errors: [
                "You need to sign in or sign up before continuing."
            ]
        });
    }

    next();
};

module.exports = {
    newToken,
    protectRoute
};