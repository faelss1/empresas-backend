const { Client } = require('pg');

//arquivo que realiza as ações relacionadas ao Postgres
module.exports = {
    connect: callback => {
        const client = new Client({
            host: process.env.PG_HOST,
            user: process.env.PG_USER,
            password: process.env.PG_PASS,
            database: process.env.PG_DB
        });
        client.connect((err, client) => {
            callback(err, client);
        });
    },
    query: (client,sql,params = [],callback) => {
        return client.query(sql ,params ,(err,values) => {
            if (err) {
                callback({error: err});
            } else {
                callback({rows: values.rows});
                client.end();
            }
        });
    }
};


