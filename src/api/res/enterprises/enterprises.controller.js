const { connect,query } = require('../../utils/db/pg');

module.exports = {
    detailEnterprises: (req, res) => {
        connect( (err,client) => {
            if (err) {
                res.status(500).json({status: 500, error: "Could not connect to postgres"});
            } else {
                const detailEnterprisesSQL = `
                    select * from enterprises.enterprises where id = $1;
                `;
                query(client, detailEnterprisesSQL, [req.params.id], db => {
                    if (!db.error) {
                        res.status(200).json({
                            enterprise: db.rows[0],
                            success: true
                        });
                    }
                });
            }
        });
    },
    listEnterprises: (req, res) => {
        connect( (err,client) => {
            if (err) {
                res.status(500).json({status: 500, error: "Could not connect to postgres"});
            } else {
                const detailEnterprisesSQL = `
                    select * from enterprises.enterprises;
                `;
                query(client, detailEnterprisesSQL, [], db => {
                    if (!db.error) {
                        res.status(200).json({
                            enterprises: db.rows
                        });
                    }
                });
            }
        });
    },
    filterEnterprises: (req, res) => {
        connect( (err,client) => {
            if (err) {
                res.status(500).json({status: 500, error: "Could not connect to postgres"});
            } else {
                const getQueryParams = req.query;
                let where = '';
                let whereParams = [];

                if (getQueryParams.name) {
                    whereParams.push('%'+getQueryParams.name+'%');
                    where = where.concat(' and enterprise_name ilike $' + whereParams.length);
                }

                if (getQueryParams.enterprise_types) {
                    whereParams.push(getQueryParams.enterprise_types);
                    where = where.concat(' and (enterprise_type->\'id\')::integer = $' + whereParams.length);
                }

                const detailEnterprisesSQL = `
                    select * from enterprises.enterprises
                    where 1=1 ${where}
                `;
                query(client, detailEnterprisesSQL, whereParams, db => {
                    if (!db.error) {
                        res.status(200).json({
                            enterprises: db.rows
                        });
                    }
                });
            }
        });
    },
    handleNotFound: (req, res) => {
        res.status(404).json({
            status: 404,
            error: "Not Found"
        });
    }
};